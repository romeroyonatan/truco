# Métodos especiales en Python

> Truco Argentino *casi* implementado con métodos especiales

## Doctests
Se pueden correr *doctest* para corroborar que los ejemplos sean correctos

### Presentación
```sh
python3 -m doctest presentacion/index.html
```

### Código de ejemplo

```sh
python3 -m doctest truco_v1/truco.py
```

```sh
# Solo Python 3.7+
python3 -m doctest truco_v2/truco.py
```
